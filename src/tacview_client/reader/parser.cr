require "big"
require "string_scanner"

module TacviewClient
  class Reader
    # Parses an event coming out of the Tacview server when they are
    # too complicated for simple extraction.
    #
    # Not to be used directly by library users, should only be called
    # by an instance of the Reader class
    #
    # See https://www.tacview.net/documentation/acmi/en/ for information
    # of the Tacview ACMI format this module is parsing
    # @private
    private class Parser
      private HEX_NUMBER                = /[0-9a-f]+/i
      private OPTIONALLY_DECIMAL_NUMBER = /\d+\.?\d+/
      private POSITION_START_INDICATOR  = /T=/
      private POSITION_SEPARATOR        = /\|/
      private FIELD_SEPARATOR           = /,/
      private END_OF_FILE               = /$/
      private END_OF_FIELD              = Regex.union(FIELD_SEPARATOR, END_OF_FILE)

      def initialize(line : String)
        @scanner = StringScanner.new(line)
        @result = {} of String => String | BigDecimal
      end

      # Parse an ACMI line associated with the update of an object.
      def parse_object_update : Hash(String, String | BigDecimal) | Nil
        parse_object_id

        return nil if own_vehicle_message?

        parse_lon_lat_alt
        parse_heading
        parse_fields

        @result
      end

      private def parse_object_id
        object_id = @scanner.scan(HEX_NUMBER)
        raise "object_id not found" unless object_id
        @result["object_id"] = object_id
        @scanner.skip FIELD_SEPARATOR
      end

      # Updates without a position are always from own vehicle based on looking
      # through samples. We don"t care about these so ignore them
      private def own_vehicle_message?
        @scanner.peek(2) != POSITION_START_INDICATOR.source
      end

      private def parse_lon_lat_alt
        @scanner.skip POSITION_START_INDICATOR

        %w[longitude latitude altitude].each do |field|
          value = @scanner.scan(OPTIONALLY_DECIMAL_NUMBER)
          @result[field] = BigDecimal.new(value) if value
          @scanner.skip POSITION_SEPARATOR
        end
      end

      private def parse_heading
        return if end_of_message?

        # Check to see if the heading (9th field which is 4 more
        # separators from our current position) is present
        separators = @scanner.check_until(END_OF_FIELD)
        if separators && separators.count(POSITION_SEPARATOR.source) == 4
          # If it is then we will save that as well by skipping all the
          # text until the heading value
          @scanner.scan_until(/\|[\|\-?0-9.]*\|/)
          heading = @scanner.scan OPTIONALLY_DECIMAL_NUMBER
          @result["heading"] = BigDecimal.new(heading) if heading
        end

        @scanner.scan_until END_OF_FIELD
      end

      private def parse_fields
        until end_of_message?
          field = @scanner.scan_until END_OF_FIELD

          if field
            field = field.chomp FIELD_SEPARATOR.source
            key, value = field.split("=", 2)
            @result[key.downcase] = value
          end
        end
      end

      private def end_of_message?
        @scanner.eos?
      end
    end
  end
end
