# tacview_client

A Crystal client that speaks the [Tacview](http://www.tacview.net) protocol and
can connect to a Tacview compatible server.

## Installation

1. Add the dependency to your `shard.yml`:

   ```yaml
   dependencies:
     tacview_client:
       gitlab: overlord-bot/tacview-crystal-client
   ```

2. Run `shards install`

## Usage

### API

To connect to a Tacview server with this library you can use the following code

```crystal

require 'tacview_client'

# See docs for all parameters
client = TacviewClient::Client.new host: 127.0.0.1,
                                   processor: ProcessorInstance
```

This shard uses Inversion Of Control principles and expects you to provide an
object to receive events for processing. This is an instance of a class that
inherits the `TacviewClient::BaseProcessor` class. See the documentation of
this class for more information.

For an example of this see the `ConsoleOutputter` class in the
`src/tacview_client/cli.cr` file

#### Logging

This shard provides no logging out of the box.

### Command-line

This shard provides a command-line application that demonstrate how to
call the code in this shard. build the command-line application using

```
crystal build src/tacview_client/cli.cr --release -o bin/tacview_client
```

then call `./bin/tacview_client --help` for invocation information. This
command-line will connect to a Tacview server and print the parsed event
stream to the terminal 

## Development

TODO: Write development instructions here

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Jeffrey Jones](https://github.com/rurounijones) - creator and maintainer
